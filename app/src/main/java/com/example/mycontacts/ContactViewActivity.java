package com.example.mycontacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class ContactViewActivity extends AppCompatActivity implements ContactViewFragment.Contract {

    public static final String EXTRA = "CVA_Contact";
    private static final String TAG = "ContactViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_view);

        if(getFragmentManager().findFragmentById(R.id.view_fragment_container) == null) {
            ContactViewFragment cvf = new ContactViewFragment();
            cvf.setPosition(getIntent().getIntExtra(EXTRA, 0));
            getFragmentManager().beginTransaction().add(R.id.view_fragment_container, cvf).commit();
        }
    }

    @Override
    public void selectEditPosition(int position) {
        Intent i = new Intent(this, ContactEditActivity.class);
        i.putExtra(ContactEditActivity.EXTRA, position);
        startActivity(i);
    }
}
