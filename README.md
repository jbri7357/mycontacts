# My Contacts #

Example address book app for Android. 

### Topics addressed: ###

* Using various views and view adapters
* Material Design standards
* Themes
* Toolbars
* Singletons
* Navigating between activities
* Fragments (creating and passing data between)